<?php


    function test_page_content($content = NULL) {
        $content = ''; //инициализируем переменную пустого контента
        $query = db_select('node_revision', 'n'); //выбираем таблицу node_revision в которой лежит текущая ревизия документа
        $query->innerJoin('field_revision_body', 'b', 'b.revision_id = n.vid'); //присоединяем таблицу field_revision_body в которой лежит body
        $query->innerJoin('node', 'd', 'n.nid=d.nid'); //присоединяем таблицу node в которой лежит title ноды
        $query->innerJoin('field_data_field_image', 'fdf', 'n.nid=fdf.entity_id');
        $query->innerJoin('file_managed', 'fm', 'fdf.field_image_fid=fm.fid');

        $query->fields('n', array('title'), array('nid'), array('vid')); //выбираем поля
        $query->fields('b', array('body_value'));
        $query->fields('fm', array('uri'));

        $query->condition('d.type', 'news'); //делаем ограничение по контент типу
        $query->orderBy('n.timestamp', 'DESC'); //сортируем  сначала свежие новости
        $query->range(0, 10); //выбираем только 10 последних
        $result = $query->execute(); //выполняем запрос к БД
        while($nodes = $result->fetch()){ //обрабатываем запрос
            $content .= '<h3>' . $nodes->title . '</h3>'; //выводим title
            $content .= $nodes->body_value; //выводим body
            $content .= '<img src="/sites/default/files/'.substr($nodes->uri,9).'">';
        }

        return $content; //возвращаем контент
    }

/**
 * Implements hook_block_info().
 */

    function test_block_info (){
        $blocks['test_block'] = array(
            'info' => t('Block News'),
            'cache' => DRUPAL_CACHE_GLOBAL,
        );
        return $blocks;
    }

/**
 * Implements hook_block_view().
 * @param string $delta
 * @return array
 */
    function test_block_view ($delta = ''){
        $block = array();

        switch ($delta){
            case 'test_block':
                $block['subject'] = t('Title');
                $block['content'] = t("Hello World");
                break;
        }
        return $block;
    }



